const express = require('express')
const bodyParser = require('body-parser')
const escpos = require('escpos')
const http = require('http')
const internetAvailable = require('internet-available')

// *** SERVIDOR ***
const app = express()
app.use(bodyParser.json({
  limit: '10mb',
  extended: true
}))
app.use(bodyParser.urlencoded({
  limit: '10mb',
  extended: true
}))

app.use(express.static(__dirname))
const server = app.listen(3000, '10.3.141.1', () => {
  let infoServe = server.address()
  console.log(`Server: http://${infoServe.address}:${infoServe.port}`)
})

// *** CAMARA ***

// Fot1
app.post('/pedirFoto', (req, res) => {
  http.get('http://10.3.141.140:80/snapshot.cgi?rate=0&user=admin&pwd=', (resp) => {
    resp.setEncoding('base64')
    var body = 'data:' + resp.headers['content-type'] + ';base64,'
    resp.on('data', (data) => { body += data })
    resp.on('end', () => { res.send(body) })
  }).on('error', (e) => {
    console.log(`Got error: ${e.message}`)
  })
})

// Control
app.post('/camControl', (req, res) => {
  http.get(`http://10.3.141.140:80/decoder_control.cgi?command=${Object.keys(req.body)[0]}&onestep=1&user=admin&pwd=`)
  res.send('finaliza')
})

app.post('/camBrillo', (req, res) => {
  http.get(`http://10.3.141.140:80/camera_control.cgi?param=1&value=${Object.keys(req.body)[0]}&user=admin&pwd=`)
  res.send('finaliza')
})

// *** IMPRESORA ***

// Recibo volqueta
app.post('/imprimir', async (req, res) => {
  const datos = req.body
  // Select the adapter based on your printer type
  const device = new escpos.USB()
  const options = { encoding: 'GB18030' }
  const printer = new escpos.Printer(device, options)
  device.open(function () {
    printer.size(1, 1)
      .align('ct')
      .style('bu')
      .text('CONINGENIERIA - MALL PLAZA')
      .style('b')
      .text(`${datos.placa} - ${datos.empresa}`)
      .text(`${datos.fecha} - ${datos.hora}`)
      .text(`${datos.m3} m3 - ${datos.codigo}`)
      .text(`dia: ${datos.dia} - corte: ${datos.corte}`)
      .text(`sitio: ${datos.dispo}`).style('bu')
      .text('COPIA VALIDA PARA VOLQUETA')
      .qrimage(`${datos.codigo}${datos.placa}`, function () {
        this.cut()
        this.close()
      })
  })
  await pausar(5000)
  res.send('finaliza')
})

// Recibo sitio de disposicion
app.post('/imprimirDispo', async (req, res) => {
  const datos = req.body
  const device = new escpos.USB()
  const options = { encoding: 'GB18030' }
  const printer = new escpos.Printer(device, options)
  device.open(function () {
    printer.size(1, 1).align('ct')
      .style('bu')
      .text('CONINGENIERIA - MALL PLAZA')
      .text(`${datos.dispo.toUpperCase()}`)
      .style('b')
      .text(`${datos.placa} - ${datos.fecha}`)
      .text(`${datos.m3} m3 - ${datos.codigo}`)
      .text('')
      .style('bu')
      .text('VALIDO PARA SITIO DE DISPOSICION')
      .qrimage(`${datos.codigo}${datos.placa}`, function () {
        this.cut()
        this.close()
      })
  })
  // evita que se produzca segundo llamado
  res.send('finaliza')
})

// *** FUNCIONES ***

function pausar (ms) {
  return new Promise(resolve => setTimeout(resolve, ms))
}

app.post('/conexion', async (req, res) => {
  try {
    await internetAvailable({ timeout: 1000, retries: 2 })
    res.send(true)
  } catch (err) {
    console.log(0)
    res.send(false)
  }
})
