import { PolymerElement, html } from '@polymer/polymer/polymer-element.js'
// import * as firebase from 'firebase'
import './shared-styles.js'

class Temp extends PolymerElement {
  static get template () {
    return html`
      <style include='shared-styles'>
      </style>

      <div class='card'>
      {{nombre}}<br>
      Cantidad: {{numero}}
      </div>
    `
  }

  static get properties () {
    return {
      nombre: String,
      numero: Number
    }
  }
}

window.customElements.define('tem-poral', Temp)
