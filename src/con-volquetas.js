/* eslint-disable no-undef */
import { PolymerElement, html } from '../node_modules/@polymer/polymer/polymer-element.js'
import firebase from '../node_modules/firebase/app/dist/index.esm.js'
import '../node_modules/@polymer/polymer/lib/elements/dom-if.js'
import '../node_modules/firebase/firestore/dist/index.esm.js'
import '../node_modules/firebase/storage/dist/index.esm.js'
import '../node_modules/@polymer/paper-input/paper-input.js'
import '../node_modules/@polymer/paper-dialog/paper-dialog.js'
import '../node_modules/@polymer/paper-radio-button/paper-radio-button.js'
import '../node_modules/@polymer/paper-radio-group/paper-radio-group.js'
import '../node_modules/@polymer/iron-ajax/iron-ajax.js'
import './opc-comprobar.js'
import './opc-informacion.js'
import './shared-styles.js'

class ConVolquetas extends PolymerElement {
  static get template () {
    return html`
      <!-- 0. ESTILOS  -->
      <style include='shared-styles'>
        /* ---  ---*/
        :host {
          display: block;
          padding: 10px;
        }
        
        .iniIngreso {
          display: grid;
          grid-template-columns: 2.5fr 0.3fr;
          justify-items:center;
          margin-top:-10px;
        }

        .busqueda {
          max-width: 145px;
          margin-top:-35px;
          --paper-input-container: {
            font-size: 18px;
            margin-left: 10px;
          }
        }

        .contenedor2Placa {
          display: flex;
          align-items: center;
          justify-content: space-around;
          margin-bottom: 5px
        }

        .iconoInfo {
          --iron-icon-height: 24px;
          --iron-icon-width: 24px;
          color: #004D40
        }

        .nuevaPlacaContenedor{
          display: none;
          margin-top:10px;
          align-items: center;
          justify-content: space-around;
          flex-wrap: wrap

        }

        .nuevaPlaca {
          width: 70px;
          --paper-input-container: {
            font-size: 18px;
          }
        }

        .nuevoM3 {
          max-width: 60px;
        }

        .nuevaPlacaCabezera {
          display:flex;
          width: 100%;
          color:black;          
          margin-bottom:5px;
          justify-content: space-between
        }

        .nuevaPlacaTitulo {
          margin:0px;
        }
        
        .placaBoton {
          background-color: white;
          font-size: 16px;
          border: 1px solid black;
          border-radius: 8%;
          margin:5px;
        }

        .contenedor {
          display: flex;
          flex-wrap: wrap;
          justify-content: space-around
        }

        .iniDatos {
          display: flex;
          flex-wrap: wrap;
          justify-content: space-between
        }

        .varDatos {
          display: flex;
          justify-content: space-between;
          flex-wrap:wrap;
          margin-top:5px;
          margin-bottom:20px
        }

        .datos {
          display: none
        }

        .m³ {
          max-width: 55px;
          color: black;
          --paper-input-container-input: {
            font-size: 18px;
          };
          --paper-input-container-label: {
            font-size: 18px;
          };
        }

        .botadero {
          --paper-input-container-label: {
            font-size: 18px;
          };
        }

        .guardar {
          color: #3F51B5;
          width: 32px;
          height: 32px;
        }

        .observaciones {
          color: black;
          width: 70%;
          --paper-input-container-label: {
            font-size: 18px;
          };
        }

        .cam {
          position: absolute;
          background-color: rgb(33, 33, 33, .7);
          color: white;
          width: 32px;
          height: 32px;
        }

        .subir {
          transform: rotate(-90deg);
          margin-left: 32px
        }
        
        .bajar {
          transform: rotate(-270deg);
          margin-top: 64px;
          margin-left: 32px
        }

        .izquierda {
          transform: rotate(-180deg);
          margin-top: 32px;
        }

        .derecha {
          margin-top: 32px;
          margin-left: 64px
        }
        
        .brilloMas {
          margin-top: 128px;
        }
        
        .brilloMenos {
          margin-top: 170px;
        }
        
        .iconoAvideo {
          position: absolute;
          background-color: rgb(33, 33, 33, .7);
          color: white
        }

        .fotoContenedor {
          width: 320px; 
          height: 240px
        }

        .fotoInicio {
          display: flex; 
          height: 100%; 
          justify-content: center; 
          align-items: center
        }

        .textoContenedor {
          position: absolute;
          display:flex;
          flex-wrap:wrap;
          margin-top: -28px;
          background-color: rgb(33, 33, 33, .7);
          color: white;
          width: 320px
        }

        .texto {
          margin: 5px;
          margin-bottom:-2px;
          margin-top:-1px;
          width: 100%
        }

        paper-dialog {
          min-width: 321px;
          max-width: 321px
        }

        .dispoControl {
          display: flex
        }

        .dispoEliminar {
          max-width: 80px;
          margin-left: 20px
        }

        .dispoContenedor {
          overflow: auto;
          height: 200px
        }
      </style>
     
      <!-- 1. INGRESO DE DATOS -->

      <!-- Ingreso de placa -->
      <div class='card'>
        <div class='iniIngreso'>
          <paper-input 
            id='busqueda' 
            class='busqueda' 
            value='{{busqueda}}' 
            placeholder='Placa' 
            on-click='_verBusqueda' 
            maxlength='6'>
            <iron-icon icon='my-icons:search' slot='prefix'></iron-icon>
            <paper-icon-button slot='suffix' on-click='_limpiar' icon='my-icons:close' style='color:#7f0000'>
            </paper-icon-button>
          </paper-input>
        </div>
        <div id='contenedor' class='contenedor'>
          <template is='dom-repeat' items={{placas}}>
            <div class='contenedor2Placa'>
              <input id='[[item]]' class='placaBoton' type='button' value='[[item]]' on-click='_ingreso'></input>
              <iron-icon id='info[[item]]' class='iconoInfo' icon='my-icons:zoom-in' on-click='_pedirInfo'></iron-icon>
            </div>
          </template>
          <array-selector id="selector" items="{{placas}}" selected="{{placaSeleccion}}"></array-selector>
        </div>
        <div id='nuevaPlacaContenedor' class='nuevaPlacaContenedor'>
          <div class='nuevaPlacaCabezera'>
            <p class='nuevaPlacaTitulo'>Registrar:</p>
            <select id='selecEmpresa'>
              <option value="Contratista">Contratista</option>
              <option value="Coningenieria">Coningenieria</option>
            </select>
            <iron-icon icon='my-icons:save' style='color:#004D40' on-click='_nuevaVolqueta'></iron-icon>
          </div>  
          <paper-input 
            id='nuevaPlaca' 
            class='nuevaPlaca' 
            value='{{nuevaPlaca}}' 
            always-float-label label='Nueva placa' 
            auto-validate
            maxlength='6'
            pattern='[a-zA-Z]{3}[0-9]{3}'
            error-message='ABC123'>
          </paper-input>
          <paper-input 
            id='nuevoM3' 
            class='nuevoM3'
            value='{{nuevoM3}}'
            type='number'
            always-float-label label='Cubicaje'>
            <div slot='suffix' style='color:black'>m³</div>
          </paper-input>
        </div>
      </div>

      <!-- Ingreso de datos y foto -->
      <div id='datos' class='card datos'>
        <div id ='iniDatos' class='iniDatos'>
          <iron-icon 
            id='favoritoIcono' 
            icon='my-icons:star-border' 
            style='color:#004D40' 
            slot='prefix'
            on-click='_favorito'></iron-icon>
          <h3 style='color:black; margin: 0'> {{placa}} | d: {{dia}} - c: {{corte}} </h3>
          <iron-icon id='guardar' class='guardar' icon='my-icons:save' on-click='_preGuardar' hidden$='{{vistaBoton}}' slot='prefix'></iron-icon>
        </div>
        <template is="dom-if" if='{{accesoDatos}}'>
          <div id='varDatos' class='varDatos'>
          <paper-input id='datosM'  class='m³' always-float-label label='Cantidad' value='{{m3}}'>
            <div slot='suffix'>m³</div>
          </paper-input>
          </paper-dropdown-menu>
          <paper-input 
            id='datosObser' 
            class='observaciones' 
            always-float-label label='Observaciones' 
            value={{observaciones}}>
          </paper-input>
          <paper-input id='datosDispo' class='observaciones' always-float-label label='Disposición' value='{{dispoElegido}}' readonly="readonly">
            <paper-icon-button slot='suffix' on-click='_dispoBuscar' icon='my-icons:search'>
            </paper-icon-button>
          </paper-input>
          </div>
          <div id='fotoContenedor' class='fotoContenedor'>
          <iron-icon id='camSubir' class='cam subir' icon='my-icons:forward' on-click='_camControl'></iron-icon>
          <iron-icon id='camBajar' class='cam bajar' icon='my-icons:forward' on-click='_camControl'></iron-icon>
          <iron-icon id='camIzquierda' class='cam izquierda' icon='my-icons:forward' on-click='_camControl'></iron-icon>
          <iron-icon id='camDerecha' class='cam derecha' icon='my-icons:forward' on-click='_camControl'></iron-icon>
          <iron-icon id='camBrilloMas' class='cam brilloMas' icon='my-icons:brightness-high' on-click='_camBrillo'></iron-icon>
          <iron-icon id='camBrilloMenos' class='cam brilloMenos' icon='my-icons:brightness-low' on-click='_camBrillo'></iron-icon>
          
          <!-- <img id='video' src="http://10.3.141.140:80/videostream.cgi?rate=0&user=admin&pwd="> -->
          <div class='textoContenedor'> 
            <p class='texto'>{{fotoTexto}}</p>
          </div>
          </div>
        </template>

      </div>
      
      <!-- Eleccion de de disposicion -->
      <paper-dialog id="dispoBuscar">
        <div class='dispoContenedor'>
          <paper-radio-group selected="small">
            <template is='dom-repeat' items={{disposicion}} mutable-data>
              <paper-radio-button name='[[item]]' on-click='_dispoEleccion'>[[index]]. [[item]]</paper-radio-button>
            </template>
          </paper-radio-group>
        </div>
        <div class='dispoControl'>
          <paper-input 
            id='datosDispo' 
            class='observaciones' 
            always-float-label label='Adicionar' 
            value='{{dispoAdicionar}}'>
            <paper-icon-button 
              slot='suffix' 
              on-click='_dispoAdicionar' 
              icon='my-icons:add-circle' 
              style='color:#3F51B5'>
            </paper-icon-button>
          </paper-input>
          <paper-input class='dispoEliminar' type='number' always-float-label label='Eliminar #' value='{{dispoEliminar}}' >
            <paper-icon-button 
              slot='suffix' 
              on-click='_dispoEliminar' 
              icon='my-icons:close' 
              style='color:#7f0000'>
            </paper-icon-button>
          </paper-input>
        </div>
      </paper-dialog>
      
      <!-- Mensaje a usuario -->
      <paper-dialog id='mensaje'>
        <p>{{mensaje}}</p>
      </paper-dialog>


      <!-- 2. OPCIONES -->
      <template is="dom-if" if="{{comprobacionGs}}">
        <opc-comprobar base-datos={{db}} ></opc-comprobar>
      </template>

      <template is="dom-if" if="{{mostrarInfo}}">
        <opc-informacion base-datos={{db}} info-placa={{infoPlaca}} info-cerrar={{mostrarInfo}} titulo-dia={{tituloDia}} titulo-m3={{tituloM3}}></opc-informacion>
      </template>
      
      <!-- 3. CAMARA -->

      <!-- Foto -->
      <iron-ajax 
        id='pedirFoto' 
        url='http://10.3.141.1:3000/pedirFoto' 
        method='post'
        handle-as="text"
        last-response={{fotoBd}}
        on-response='_guardar'>
      </iron-ajax>

      <!-- Control posicion -->
      <iron-ajax 
        id='camControl' 
        url='http://10.3.141.1:3000/camControl'
        handle-as="text"
        method='post'
        body={{comando}}>
      </iron-ajax>
      
      <!-- Control brillo -->
      <iron-ajax 
        id='camBrillo' 
        url='http://10.3.141.1:3000/camBrillo'
        method='post'
        handle-as="text"
        body={{brillo}}>
      </iron-ajax>

      <!-- 4. RECIBOS -->
     
      <!-- Recibo volqueta -->
      <iron-ajax 
        id='imprimir' 
        url='http://10.3.141.1:3000/imprimir' 
        method='post'
        content-type="application/json"
        body={{infoRecibo}}
        on-response='_imprimirDispo'>
      </iron-ajax> 
      
      <!-- Recibo disposicion -->
      <iron-ajax 
        id='imprimirDispo' 
        url='http://10.3.141.1:3000/imprimirDispo' 
        method='post'
        content-type="application/json"
        body={{infoDispo}}>
      </iron-ajax>

      <!-- Chequeo de la conexion -->
      <iron-ajax 
        id='conexion' 
        url='http://10.3.141.1:3000/conexion' 
        method='post'
        last-response={{conexion}}>
      </iron-ajax>
    `
  }

  static get properties () {
    return {
      busqueda: {
        type: String,
        observer: '_buscar'
      },
      fotoVista: {
        type: Boolean,
        value: true
      },
      foto: {
        type: Boolean,
        value: false
      },
      observaciones: {
        type: String,
        value: ''
      },
      tituloDia: {
        type: Number,
        notify: true
      },
      tituloM3: {
        type: Number,
        notify: true
      },
      vectorReg: {
        type: Array,
        value: [
          'a', 'Y', '%', 'o', 'E', '5', 'w', 't', '_', 'U', '~', 'S', 'H', '{',
          'r', 'x', 'g', 'l', 'n', 'A', 'L', 'b', '+', 'C', '@', '3', '1', '4',
          '^', '9', 'd', 'q', 'Q', 'G', 'K', 'u', 'T', 'v', '8', 'B', 's', 'k',
          '&', 'R', ';', 'W', '$', 'P', '}', 'J', '*', ',', 'z', 'h', 'X', '<',
          'i', 'y', 'M', '[', '>', '?', '=', 'I', ')', '¡', 'Z', 'm', ']', 'F',
          '|', '0', 'e', '!', '6', 'D', 'f', '-', '(', '2', 'N', 'p', 'V', 'c',
          '.', 'O', ':', '7'
        ]
      },
      eliminarViaje: {
        type: String,
        observer: '_eliminar'
      },
      eliminarNota: {
        type: String,
        notify: true
      },
      brillo: {
        type: Number,
        value: 64
      },
      pruebaImpresora: {
        type: Boolean,
        observer: '_pruebaImpresora',
        notify: true
      },
      abrirOpciones: Boolean,
      comprobacionGs: Boolean,
      mostrarInfo: {
        type: Boolean,
        value: false
      },
      botonComprobar: {
        type: Boolean,
        value: true
      },
      textoComprobar: {
        type: String,
        value: 'Comprobar'
      },
      datosNoSincronizados: {
        type: Boolean,
        value: false
      },
      vistaBoton: {
        type: Boolean,
        value: false
      }
    }
  }

  _pedirInfo (e) {
    // informacion de placa
    this.infoPlaca = e.target.id.slice(4) // mostrar ventana

    this.mostrarInfo = true
  }

  // *** Inicio de la aplicacion ***
  async ready () {
    super.ready() // dia por defecto

    const opciones = {
      year: 'numeric',
      month: '2-digit',
      day: '2-digit'
    }
    this.diaDefecto = new Date().toLocaleDateString('ja-JP', opciones).replace(/\//g, '-')

    this._firebase() // Estado de la conexion

    await this._conexion()
  }

  async _firebase () {
    firebase.initializeApp({
      apiKey: 'AIzaSyAYNrBMc_v6fDk6qRAJPsavZMwPWZi10NI',
      authDomain: 'volquetas-1.firebaseapp.com',
      databaseURL: 'https://volquetas-1.firebaseio.com',
      projectId: 'volquetas-1',
      storageBucket: 'volquetas-1.appspot.com',
      messagingSenderId: '182689894492'
    })
    this.db = firebase.firestore()
    this.db.settings({
      timestampsInSnapshots: true
    })
    this.db.enablePersistence()
    this.almacen = firebase.storage().ref()
    const [fav, doc, reg, corteDia, dispo] = await Promise.all(
      [
        this.db.collection('listas').doc('favoritos').get(),
        this.db.collection('volquetas').get(),
        this.db.collection('listas').doc('registro').get(),
        this.db.collection('listas').doc('cortes').get(),
        this.db.collection('disposicion').get()
      ]
    ) // Inicio de datos

    this.numCorte = corteDia.data().numCorte // control de viajes

    await this._cortes(doc, dispo) // favoritos

    this.placas = this.favoritos = fav.data().lista // datos

    this.placasBd = doc.docs.map(x => {
      return {
        'placa': x.id,
        'm3': x.data().m3,
        'empresa': x.data().empresa
      }
    }) // valores diarios

    const valDia = await this.db.collection('listas').doc('dia').get()
    this.tituloDia = valDia.data().viajes
    this.tituloM3 = valDia.data().m3 // registro

    this.registro = reg.data().lista // disposicion

    this.disposicion = dispo.docs.map(x => x.id)
  }

  async _cortes (doc, dispo) {
    // const diaPrueba = new Date(2019, 2, 28)
    // const diaLocal = diaPrueba.getTime()
    // Comprobación día
    const diaLocal = new Date(new Date().setHours(0, 0, 0, 0)).getTime()
    const datos = await this.db.collection('listas').doc('cortes').get()
    const diaBd = datos.data().dia.toDate().setHours(0, 0, 0, 0)
    const batch = this.db.batch()
    // reinicio dia y viajes diarios en Bd
    if (diaLocal > diaBd) {
      batch.update(this.db.collection('listas').doc('cortes'), {
        // dia: diaPrueba
        dia: new Date(diaLocal)
      })
      doc.docs.map(x => batch.update(x.ref, {
        dia: 0
      }))
      dispo.docs.map(x => batch.update(x.ref, {
        dia: 0
      }))
      batch.update(this.db.collection('listas').doc('dia'), {
        m3: 0,
        viajes: 0
      })
    }
    // ** Comprobacion de cortes
    // const dia = diaPrueba.getDate()
    const dia = new Date().getDate()

    // Corte volquetas
    const corte = datos.data().cortes

    if (dia === corte[0] || dia === corte[1]) {
      if (datos.data().estadoCorte) {
        // reinicio cortes en Bd
        doc.docs.map(x => batch.update(x.ref, {
          corte: 0
        }))
        dispo.docs.map(x => batch.update(x.ref, {
          corte: 0
        })) // Se aumenta contador de cortes

        this.db.collection('listas').doc('cortes').update({
          numCorte: Number(this.numCorte) + 1,
          estadoCorte: false
        })
      }
    }

    if (dia !== corte[0] && dia !== corte[1]) {
      this.db.collection('listas').doc('cortes').update({
        estadoCorte: true
      })
    }
    batch.commit()
    // this._consultas(1)
  }

  async _consultas (tipo) {
    switch (tipo) {
      case 1:
        const consulta = await this.db.collection('volquetas').where('dia', '>', 0).get()
        const respuesta = consulta.docs.map(x => `${x.id}-${x.data().dia}`).sort()
        respuesta.map(x => console.log(x))
        break

      case 2:
        const consulta2 = await this.db.collection('volquetas').where('dia', '>', 0).get()
        const respuesta2 = consulta2.docs.map(x => `${x.id}-${x.data().m3}`).sort()
        respuesta2.map(x => console.log(x))
        break

      case 3:
        const consulta3 = await this.db.collection('datos').where('placa', '==', 'TMO999').get() // consulta.docs.map(x=>console.log())

        consulta3.docs.map(x => console.log(`${x.id}-${x.data().tiempo.toDate()}`)) // consulta.docs.map(x=>console.log(x.data()))
        break
    } // Temporal individual
  }

  // *** Buscador de placas ***
  _verBusqueda () {
    this.$.contenedor.style.display = 'flex'
    this.$.datos.style.display = 'none'
    this.dia = ''
    this.corte = ''
    this.accesoDatos = false
  }

  async _ingreso (e) {
    this.placa = e.currentTarget.value // Estado de la conexion

    await this._conexion()
    this.$.contenedor.style.display = 'none'
    this.$.nuevaPlacaContenedor.style.display = 'none'
    this.$.datos.style.display = 'block' // titulo

    this.favoritos.includes(this.placa) ? this.$.favoritoIcono.icon = 'my-icons:star' : this.$.favoritoIcono.icon = 'my-icons:star-border' // cubicaje por defecto

    const datos = this.placasBd.find(x => x.placa === this.placa)
    this.m3 = datos.m3
    this.empresa = datos.empresa
    // # salidas volquetas
    const viajes = await this.db.collection('volquetas').doc(this.placa).get()
    this.dia = viajes.data().dia
    this.corte = viajes.data().corte
    this.total = viajes.data().total
    this.accesoDatos = true // Texto en foto
    // this.tiempo = new Date(2019, 2, 28)

    this.tiempo = new Date()
    this.fecha = ` ${this.tiempo.getDate()}/${this.tiempo.getMonth() + 1}/${this.tiempo.getFullYear()} `
    this.hora = ` ${this.tiempo.getHours()}:${this.tiempo.getMinutes()}:${this.tiempo.getSeconds()} `
    this.fotoTexto = `Viajes: ${this.dia + 1} día 🔹 ${this.corte + 1} corte `
  }

  _buscar () {
    // se muestra favoritos
    const textoPlaca = this.busqueda.toUpperCase()
    this.nuevaPlaca = textoPlaca

    if (this.busqueda === '') {
      this.placas = this.favoritos
      this.$.nuevaPlacaContenedor.style.display = 'none'
    } else {
      // se muestra busqueda
      const placasFiltradas = this.placasBd.filter(x => x.placa.includes(textoPlaca))

      if (placasFiltradas.length > 0) {
        this.placas = placasFiltradas.map(x => x.placa)
        this.$.nuevaPlacaContenedor.style.display = 'none'
      } else {
        // se muestra ingreso de nueva placa
        this.placas = ''
        this.$.nuevaPlacaContenedor.style.display = 'flex'
      }
    }
  }

  _nuevaVolqueta () {
    this.nuevaPlaca = this.nuevaPlaca.toUpperCase()
    const patronPlaca = new RegExp('[a-zA-Z]{3}[0-9]{3}')

    if (patronPlaca.test(this.nuevaPlaca)) {
      const empresa = this.$.selecEmpresa
      this.db.collection('volquetas').doc(this.nuevaPlaca).set({
        corte: 0,
        dia: 0,
        empresa: empresa.options[empresa.selectedIndex].text,
        m3: Number(this.nuevoM3),
        total: 0
      }) // termino proceso

      this.placasBd.push({
        placa: this.nuevaPlaca,
        m3: Number(this.nuevoM3),
        empresa: empresa.options[empresa.selectedIndex].text
      })
      this.nuevaPlaca = this.nuevoM3 = this.busqueda = ''
      this.$.nuevaPlacaContenedor.style.display = 'none'
    } else {
      // eslint-disable-next-line no-undef
      this.mensaje = 'Revisar información de ingreso'
      this.$.mensaje.open()
    }
  }

  _favorito () {
    // Se quita de favoritos
    if (this.favoritos.includes(this.placa)) {
      this.favoritos = this.favoritos.filter(x => x !== this.placa)
      this.db.collection('listas').doc('favoritos').set({
        lista: this.favoritos
      })
      this.$.favoritoIcono.icon = 'my-icons:star-border'
    } else {
      // se agrega a favoritos
      this.favoritos.push(this.placa)
      this.db.collection('listas').doc('favoritos').set({
        lista: this.favoritos
      })
      this.$.favoritoIcono.icon = 'my-icons:star'
    } // actualizo vista

    this.placas = this.favoritos
  }

  // *** Disposicion ***
  _dispoBuscar () {
    this.$.dispoBuscar.open()
  }

  _dispoEleccion (e) {
    this.dispoElegido = e.target.name
    this.$.dispoBuscar.close()
  }

  _dispoAdicionar () {
    if (this.dispoAdicionar !== '') {
      this.push('disposicion', this.dispoAdicionar)
      this.db.collection('disposicion').doc(this.dispoAdicionar).set({
        corte: 0,
        dia: 0,
        total: 0
      })
    }

    this.dispoAdicionar = ''
  }

  _dispoEliminar () {
    if (this.dispoEliminar !== '' && this.dispoEliminar !== undefined && this.dispoEliminar <= this.disposicion.length && Number(this.dispoEliminar) >= 0) {
      this.db.collection('disposicion').doc(this.disposicion[this.dispoEliminar]).delete()
      this.splice('disposicion', this.dispoEliminar, 1)
    }
  }

  // *** Foto ***
  _camControl (e) {
    const eleccion = e.target.id
    const comandoPos = ['camSubir', '', 'camBajar', '', 'camIzquierda', '', 'camDerecha']
    this.comando = comandoPos.indexOf(eleccion).toString()
    this.$.camControl.generateRequest()
  }

  _camBrillo (e) {
    const eleccion = e.target.id
    eleccion === 'camBrilloMas' ? this.set('brillo', Number(this.brillo) + 16) : this.set('brillo', Number(this.brillo) - 16)
    this.set('brillo', this.brillo.toString())
    this.$.camBrillo.generateRequest()
  }

  // *** Impresora ***
  _imprimirDispo () {
    this.infoDispo = {
      dispo: this.dispoElegido,
      placa: this.placa,
      fecha: this.fecha,
      dia: this.diaDispo,
      corte: this.corteDispo,
      m3: this.m3,
      codigo: this.unico
    }
    this.$.imprimirDispo.generateRequest()
  }

  _imprimir () {
    // informacion
    this.infoRecibo = {
      codigo: this.unico,
      placa: this.placa,
      empresa: this.empresa,
      fecha: this.fecha,
      hora: this.hora,
      dia: Number(this.dia) + 1,
      corte: Number(this.corte) + 1,
      dispo: this.dispoElegido,
      m3: this.m3
    }
    this.$.imprimir.generateRequest()
  }

  // *** Funcionalidades ***
  _preGuardar () {
    // se esconde
    this.$.guardar.hidden = true
    // this.$.pedirFoto.generateRequest();
    this._guardar()
  }

  async _guardar () {
    if (this.dispoElegido === '') {
      this.$.guardar.style.color = '#3F51B5'
      this.mensaje = 'Completar información'
      this.$.mensaje.open()
      return
    }
    // Se genera registro unico
    // eslint-disable-next-line
    const f = x => i => x[i] + 1 <= this.vectorReg.length
      ? (x[i] = x[i] + 1, x) : (x[i] = 0, f(x)(i - 1))
    this.registro = f(this.registro)(3)
    this.unico = this.registro.map(x => this.vectorReg[x]).join('')
    // se registra en Firestore
    this.db.collection('listas').doc('registro').set({
      lista: this.registro
    })
    // se guarda la info
    this.db.collection('datos').doc(this.unico).set({
      codigo: this.unico,
      placa: this.placa,
      tiempo: this.tiempo,
      dia: Number(this.dia) + 1,
      corte: Number(this.corte) + 1,
      disposicion: this.dispoElegido,
      observaciones: this.observaciones,
      m3: Number(this.m3),
      empresa: this.empresa,
      numCorte: this.numCorte,
      eliminado: ''
    })
    // guarda foto localmente
    // Se envia Storage
    // const puntoMontaje = this.almacen.child(`fotos/${this.unico}.png`);
    // puntoMontaje.putString(this.fotoBd.slice(23), 'base64'); // actualiza volqueta

    this.db.collection('volquetas').doc(this.placa).update({
      dia: Number(this.dia) + 1,
      corte: Number(this.corte) + 1,
      total: Number(this.total) + 1
    })
    // actualiza disposicion
    const tempDispo = await this.db.collection('disposicion').doc(this.dispoElegido).get()
    const {
      corte,
      dia,
      total
    } = tempDispo.data()
    this.db.collection('disposicion').doc(this.dispoElegido).update({
      dia: Number(dia) + 1,
      corte: Number(corte) + 1,
      total: Number(total) + 1
    })
    this.diaDispo = Number(dia) + 1
    this.corteDispo = Number(corte) + 1
    // imprime recibo
    this._imprimir()
    // actualiza contador local
    this.tituloDia++
    this.tituloM3 = Number(this.tituloM3) + Number(this.m3)
    this.db.collection('listas').doc('dia').set({
      m3: this.tituloM3,
      viajes: this.tituloDia
    })
    // Vista
    // se muestra boton
    this.$.guardar.hidden = false
    this._verBusqueda()
  }

  async _conexion () {
    await this.$.conexion.generateRequest().completes // this.conexion ? this.db.enableNetwork().then(this._mensajeConexion('✔ En linea')) : this.db.disableNetwork().then(this._mensajeConexion('🚫 Sin linea '));

    this.conexion ? this._mensajeConexion('✔ Conectado') : this._mensajeConexion('🚫 Sin conexión ') // true ? this._mensajeConexion('✔ Conectado') : this._mensajeConexion('🚫 Sin conexión ')
  }

  _mensajeConexion (estado) {
    if (estado !== this.estado) {
      this.estado = estado
      this.mensaje = this.estado
      this.$.mensaje.open()
    }
  }

  _pruebaImpresora () {
    if (this.pruebaImpresora) {
      this.infoDispo = {
        dispo: 'Prueba',
        placa: 'AAA111',
        fecha: 'Actual',
        dia: 1,
        corte: 1,
        m3: 1,
        codigo: 'aaaa'
      }
      this.$.imprimirDispo.generateRequest()
      this.pruebaImpresora = !this.pruebaImpresora
    }
  }

  _limpiar () {
    this.busqueda = ''
  }
}

window.customElements.define('con-volquetas', ConVolquetas)
