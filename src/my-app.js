import { PolymerElement, html } from "../node_modules/@polymer/polymer/polymer-element.js";
import { setPassiveTouchGestures, setRootPath } from "../node_modules/@polymer/polymer/lib/utils/settings.js";
import "../node_modules/@polymer/app-layout/app-drawer/app-drawer.js";
import "../node_modules/@polymer/app-layout/app-drawer-layout/app-drawer-layout.js";
import "../node_modules/@polymer/app-layout/app-header/app-header.js";
import "../node_modules/@polymer/app-layout/app-header-layout/app-header-layout.js";
import "../node_modules/@polymer/app-layout/app-scroll-effects/app-scroll-effects.js";
import "../node_modules/@polymer/app-layout/app-toolbar/app-toolbar.js";
import "../node_modules/@polymer/app-route/app-location.js";
import "../node_modules/@polymer/app-route/app-route.js";
import "../node_modules/@polymer/iron-pages/iron-pages.js";
import "../node_modules/@polymer/iron-selector/iron-selector.js";
import "../node_modules/@polymer/paper-icon-button/paper-icon-button.js";
import './my-icons.js';
import './con-volquetas.js'; // Gesture events like tap and track generated from touch will not be
// preventable, allowing for better scrolling performance.

setPassiveTouchGestures(true); // Set Polymer's root path to the same value we passed to our service worker
// in `index.html`.

setRootPath(MyAppGlobals.rootPath);

class MyApp extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          --app-primary-color: #4285f4;
          --app-secondary-color: black;

          display: block;
        }

        app-drawer-layout:not([narrow]) [drawer-toggle] {
          display: none;
        }

        app-header {
          color: #fff;
          background-color: var(--app-primary-color);
        }

        app-header paper-icon-button {
          --paper-icon-button-ink-color: white;
        }

        .navegadorResumen {          
          --app-drawer-width: 300px;
        }

        .contenedor {
          display: flex;
          flex-wrap:wrap;
          height: 100%; 
          overflow: auto;
        }

        .iconosOpciones{
          --iron-icon-height: 26px;
          --iron-icon-width: 26px;
        }

        .separador{
          margin-left: 8px;
          margin-right: 8px
        }

        .eliminarBoton {
          background-color: #008CBA; /* Green */
          border: none;
          color: white;
          padding: 5px 10px;
          text-align: center;
          font-size: 16px;
          border-radius: 8px
        }

        .separadores {
          margin-left: 4px;
          margin-right: 4px
        }

        .checkmark {
          height: 25px;
          width: 25px;
          background-color: #eee;
          border-radius: 50%;
        }

      </style>

      <app-location route="{{route}}" url-space-regex="^[[rootPath]]">
      </app-location>

      <app-route route="{{route}}" pattern="[[rootPath]]:page" data="{{routeData}}" tail="{{subroute}}">
      </app-route>

      <app-drawer-layout fullbleed narrow="{{narrow}}">

        <!-- Main content -->
        <app-header-layout has-scrolling-region>

          <app-header slot="header" condenses reveals effects="waterfall">
            <app-toolbar>
              <div main-title="">Mall: [[tituloDia]] viajes, [[tituloM3]] m³ </div>
              <iron-icon class='iconosOpciones' icon='my-icons:compare-arrows' on-click='_abrirComprobacionGS'></iron-icon>
              <p class='separador'>|</p>
              <iron-icon class='iconosOpciones' icon='my-icons:print' on-click='_pruebaImpresora'></iron-icon>
            </app-toolbar>
          </app-header>

          <iron-pages selected="[[page]]" attr-for-selected="name" role="main">
            <con-volquetas 
              name="volquetas" 
              titulo-dia={{tituloDia}} 
              titulo-m3={{tituloM3}}
              prueba-impresora={{pruebaImpresora}}
              comprobacion-gs={{comprobacionGS}}>
            </con-volquetas>
            <my-view404 name="view404"></my-view404>
          </iron-pages>
        </app-header-layout>
      </app-drawer-layout>
    `;
  }

  static get properties() {
    return {
      page: {
        type: String,
        reflectToAttribute: true,
        observer: '_pageChanged'
      },
      routeData: Object,
      subroute: Object,
      isNarrow: Boolean,
      pruebaImpresora: {
        type: Boolean,
        value: false
      },
      comprobacionGS: {
        type: Boolean,
        value: false
      }
    };
  }

  static get observers() {
    return ['_routePageChanged(routeData.page)'];
  }

  _ingreso() {
    this.$.drawer.toggle();
  }

  _abrirComprobacionGS() {
    this.comprobacionGS = !this.comprobacionGS;
  }

  _pruebaImpresora() {
    this.pruebaImpresora = !this.pruebaImpresora;
  } // Manejo Polymer


  _routePageChanged(page) {
    // Show the corresponding page according to the route.
    //
    // If no page was found in the route data, page will be an empty string.
    // Show 'volquetas' in that case. And if the page doesn't exist, show 'view404'.
    if (!page) {
      this.page = 'volquetas';
    } else if (['volquetas'].indexOf(page) !== -1) {
      this.page = page;
    } else {
      this.page = 'view404';
    }
  }

  _pageChanged(page) {
    // Import the page component on demand.
    //
    // Note: `polymer build` doesn't like string concatenation in the import
    // statement, so break it up.
    switch (page) {
      case 'volquetas':
        import('./con-volquetas.js');
        break;

      case 'view404':
        import('./my-view404.js');
        break;
    }
  }

}

window.customElements.define('my-app', MyApp);
