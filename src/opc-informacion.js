/* eslint-disable no-undef */
import { PolymerElement, html } from '../node_modules/@polymer/polymer/polymer-element.js'
import './shared-styles.js'

class OpcInformacion extends PolymerElement {
  static get template () {
    return html`
      <!-- 0. ESTILOS  -->
      <style include='shared-styles'>
        /* ---  ---*/
        :host {
          display: block;
        }

        .contTituloCerrar{
          display:grid;
          grid-template-columns: 0.5fr 1fr 0.8fr;
          align-items: center;
          margin-top:-12px;
        }

        .titulo{
          text-align: center;
          font-size: 16px;
          font-weight: bold;
          color: black
        }

        .encabezado{
          display: grid;
          grid-template-columns: 1fr 1fr 1fr 1fr;
          color: black;
          text-align: center;
          justify-items: center;
          align-items: center;
          margin: 2px;
          margin-top:8px;
          width: 100%;
          height: 50px;
          border-bottom-style: solid;
          border-bottom-color: #37474F
        }

        .comparacionFecha{
          max-width: 140px;
        }

        .iconoBuscar{
          margin-right: 8px;
          color: var(--icono-comprobar-color, rgba(97,97,97))
        }

        .contFechaProcesar{
          display:flex;
          justify-content: space-between;
          align-items: center;
          flex-wrap:wrap
        }

        .obtenerBoton {
          display:flex;
          flex-wrap:wrap;
          width:50px;
          justify-content: center;
          margin-right: 8px
        }

        .datos{
          border-bottom-style: none
        }

        .botonCodigo {
          border: none;
          color: white;
          text-align: center;
          font-size: 14px;
          margin: 4px 2px;
          cursor: pointer;
          height: 22px;
          width: 50px;
          border-radius: 8px
        }

        .iconoInfo {
          color: var(--icono-informacion-color, rgba(97,97,97))
        }

        .contenedorbotonBorrar{
          width: 80%;
          display: flex;
          justify-content: center
        }

        .botonBorrar {
          background-color: #607D8B;
          color: white;
          border-radius: 10%;
          border: none;
          padding: 5px
        }

      </style>

      <div class='card'>
        <div class='contTituloCerrar'>
          <iron-icon class='iconoBuscar' icon='my-icons:center-focus-strong' on-click='_infoPlaca'></iron-icon>
          <paper-input id='diaAnalisis' class='comparacionFecha' label='Fecha' type='date' value={{diaDefecto}}>
          </paper-input>
          <p class='titulo' on-click='_cerrar'>{{infoPlaca}} ❌</p>
        </div>
        <div class='encabezado'>
          <p>Código</p>
          <p>Hora</p>
          <p>Día</p>
          <p>Corte</p>
        </div>
        <template is="dom-repeat" items="{{datosPlaca}}">
          <div class='encabezado datos'>
            <p id=[[item.codigo]] 
              class='botonCodigo' 
              style='background-color: [[item.eliminado]]'
              on-click='_infoCodigo'>[[item.codigo]]
            </p>
            <p>[[item.hora]]</p>
            <p>[[item.dia]]</p>
            <p>[[item.corte]]</p>
          </div>
        </template>
      </div>

      <paper-dialog id="infoBorrar">
        <p><b>Placa:</b> [[datosCodigo.placa]]</p>
        <p><b>Código:</b> [[datosCodigo.codigo]]</p>
        <p><b>m3:</b> [[datosCodigo.m3]]</p>
        <p><b>Fecha:</b> [[datosCodigo.tiempo]]</p>
        <p><b>dia:</b> [[datosCodigo.dia]]</p>
        <p><b>corte:</b> [[datosCodigo.corte]]</p>
        <p><b>Disposición:</b> [[datosCodigo.disposicion]]</p>
        <p><b>Empresa:</b> [[datosCodigo.empresa]]</p>
        <p><b># corte:</b> [[datosCodigo.numCorte]]</p>
        <p><b>Observaciones:</b> [[datosCodigo.observaciones]]</p>
        <p><b>Eliminado:</b> [[datosCodigo.eliminado]]</p>
        <template is="dom-if" if="[[datosCodigo.botonEliminar]]" >
          <div class='contenedorbotonBorrar'>
            <input id='diaAnalisis' class='botonBorrar' type='button' value='Borrar código' on-click='_borrarCodigo'></input>
          </div>
        </template>

      </paper-dialog>
    `
  }

  static get properties () {
    return {
      baseDatos: Object,
      infoPlaca: String,
      codigo: String,
      infoCerrar: {
        type: Boolean,
        notify: true
      },
      contador: Number,
      filtroPlaca: Object,
      opcionesFecha: {
        type: Object,
        value: {
          year: 'numeric',
          month: '2-digit',
          day: '2-digit',
          hour12: false,
          hour: '2-digit',
          minute: '2-digit'
        }
      },
      tituloDia: {
        type: Number,
        notify: true
      },
      tituloM3: {
        type: Number,
        notify: true
      }
    }
  }

  async _infoPlaca () {
    this.updateStyles({
      '--icono-comprobar-color': 'blue'
    })
    const diaYy = this.diaDefecto.split(/\D/g)
    // valores de fecha
    const inicio = new Date(diaYy.join())
    const fin = new Date(inicio.getTime() + 24 * 60 * 60 * 1000)
    // datos FS (maximo 2 filtros where)
    const datosFS = await this.baseDatos.collection('datos').where('tiempo', '>', inicio).where('tiempo', '<', fin).get()
    const datosCompletos = datosFS.docs.map(x => x.data())
    this.filtroPlaca = datosCompletos.filter(x => x.placa === this.infoPlaca)
    // Obtener fecha hora -> se remueve fecha (split) y espacio (slice)
    const hora = x => x.toDate().toLocaleDateString('eu-ES', this.opcionesFecha).split(' ')[1].slice(0)

    const estiloEliminado = x => x === '' ? '#008CBA' : '#ef5350'

    this.datosPlaca = this.filtroPlaca.map(x => {
      return {
        codigo: x.codigo,
        hora: hora(x.tiempo),
        dia: x.dia,
        corte: x.corte,
        eliminado: estiloEliminado(x.eliminado)
      }
    })
    this.updateStyles({
      '--icono-comprobar-color': 'rgba(97,97,97)'
    })
  }

  _infoCodigo (e) {
    const datos = this.filtroPlaca.filter(x => x.codigo === e.target.id)[0]
    const fechaEliminado = x => x === '' ? '' : x.toDate().toLocaleDateString('eu-ES', this.opcionesFecha).replace(' ', ', ')
    this.datosCodigo = {
      codigo: datos.codigo,
      corte: datos.corte,
      dia: datos.dia,
      disposicion: datos.disposicion,
      eliminado: fechaEliminado(datos.eliminado),
      empresa: datos.empresa,
      m3: datos.m3,
      numCorte: datos.numCorte,
      observaciones: datos.observaciones,
      placa: datos.placa,
      tiempo: datos.tiempo.toDate().toLocaleDateString('eu-ES', this.opcionesFecha).replace(' ', ', '),
      botonEliminar: datos.eliminado === ''
    }
    this.$.infoBorrar.open() // si el codigo es del dia, se resta al dia si no, NO
  }

  async _borrarCodigo () {
    // ** Actualiza datos de FireStore
    // Eliminar codigo
    const batch = this.baseDatos.batch()
    // se registra accion
    batch.update(this.baseDatos.collection('datos').doc(this.datosCodigo.codigo), {
      eliminado: new Date()
    })
    // Eliminar en lugar de disposicion
    const dispoT = await this.baseDatos.collection('disposicion').doc(this.datosCodigo.disposicion).get()
    const dispo = dispoT.data()
    batch.update(this.baseDatos.collection('disposicion').doc(this.datosCodigo.disposicion), {
      dia: dispo.dia - 1,
      corte: dispo.corte - 1,
      total: dispo.total - 1
    })
    // Eliminar en volquetas
    const borrarVolquetaDia = this.datosCodigo.tiempo.split(',')[0] === this.diaDefecto ? 1 : 0
    const volqT = await this.baseDatos.collection('volquetas').doc(this.datosCodigo.placa).get()
    const volq = volqT.data()
    batch.update(this.baseDatos.collection('volquetas').doc(this.datosCodigo.placa), {
      dia: volq.dia - borrarVolquetaDia,
      corte: volq.corte - 1,
      total: volq.total - 1
    })
    // **elimina dia si es actual
    if (borrarVolquetaDia) {
      // Actualizo en FireStore
      batch.update(this.baseDatos.collection('listas').doc('dia'), {
        m3: this.tituloM3 - this.datosCodigo.m3,
        viajes: this.tituloDia - 1
      })
      this.tituloDia--
      this.tituloM3 = Number(this.tituloM3) - this.datosCodigo.m3
    }
    batch.commit()
    alert('Código borrado')
  }

  _cerrar () {
    this.infoCerrar = false
  }

  async ready () {
    super.ready() // dia por defecto

    const opciones = {
      year: 'numeric',
      month: '2-digit',
      day: '2-digit'
    }
    this.diaDefecto = new Date().toLocaleDateString('ja-JP', opciones).replace(/\//g, '-')
  }
}

window.customElements.define('opc-informacion', OpcInformacion) // async _eliminar () {
//   const batch = this.db.batch() // se registra accion
//   batch.update(this.db.collection('datos').doc(this.eliminarViaje), {
//     eliminado: new Date()
//   })
//   batch.update(this.db.collection('disposicion').doc(val.disposicion), {
//     dia: dispo.dia - 1,
//     corte: dispo.corte - 1,
//     total: dispo.total - 1
//   })
//   batch.update(this.db.collection('volquetas').doc(val.placa), {
//     dia: volq.dia - 1,
//     corte: volq.corte - 1,
//     total: volq.total - 1
//   })
//   batch.commit()
// }
