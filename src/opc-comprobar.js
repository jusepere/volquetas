/* eslint-disable no-undef */
import { PolymerElement, html } from '../node_modules/@polymer/polymer/polymer-element.js'
import './shared-styles.js'

class OpcComprobar extends PolymerElement {
  static get template () {
    return html`
      <!-- 0. ESTILOS  -->
      <style include='shared-styles'>
        /* ---  ---*/
        :host {
          display: block;
        }

        .menu {
          display: flex;
          justify-content: space-between
        }

        .comparacionFecha{
          max-width: 150px;
        }

        .comparacionInicio{
          display:flex;
          justify-content: space-between;
          align-items: center;
          flex-wrap:wrap
        }

        .comparacionBoton{
          display:flex;
          flex-wrap:wrap;
          width:50px;
          justify-content: center;
          margin-right: 8px
        }

        .iconoSincronizado{
          color: green
        }

        .iconoReporteRevision{
          color: #FFC107
        }

        .datosNoSincroTitulo{
          display: grid;
          grid-template-columns: 1fr 1fr 1fr;
          color: black;
          text-align: center;
          font-weight: bold;
          margin: 5px;
          margin-top:12px;
          width: 100%;
          border-bottom-style: solid;
          border-bottom-color: #37474F
        }

        .listaRespuesta{
          border-bottom-style: none
        }
        /* --- Ingreso ---*/
        .iconomenu {
          --iron-icon-height: 28px;
          --iron-icon-width: 28px;
          color: #455A64
        }

        .iconocomprobar{
          color: var(--icono-comprobar-color, rgba(97,97,97))
        }

        .iconoGS {
          color: #4CAF50
        }

        .iconoFS {
          color: #FFC107;

        }

        .botonPlaca {
            background-color: #008CBA; /* Green */
            border: none;
            color: white;
            text-align: center;
            font-size: 14px;
            margin: 4px 2px;
            cursor: pointer;
            height: 25px;
            width: 70px;
            border-radius: 8px
        }

      </style>

      <!-- 2. OPCIONES -->
      
      <!-- Comparación con GS -->
      <div class='card comparacionInicio'>
        <!-- Elementos de analisis -->
        <paper-input id='diaAnalisis' class='comparacionFecha' label='Fecha' type='date' value={{diaDefecto}}></paper-input>
        <template is="dom-if" if="{{botonComprobar}}">
            <div class='comparacionBoton'>
              <iron-icon id ='iconoComprobar' class='iconomenu iconocomprobar' icon='my-icons:center-focus-strong' on-click='_comprobacion'></iron-icon>
              {{textoComprobar}}
            </div>
        </template>
        <!-- Respuesta: datos sincronizados -->
        <template is="dom-if" if="{{datosSincronizados}}">
              <div class='comparacionBoton'>
                <iron-icon class='iconoSincronizado' icon='my-icons:check-box' on-click='_reiniciarComprobacion'></iron-icon>
                sincronizado
              </div>
        </template>
        <!-- Respuesta: datos sincronizados -->
        <template is="dom-if" if="{{reportarRevision}}">
            <div class='comparacionBoton'>
              <iron-icon class='iconoReporteRevision' icon='my-icons:report' on-click='_reiniciarComprobacion'></iron-icon>
              Revisar
            </div>
        </template>
        <!-- Respuesta: datos NO sincronizados -->
        <template is="dom-if" if="{{datosNoSincronizados}}">
          <div class='datosNoSincroTitulo'>
            <p>Placa</p>
            <p><iron-icon class='iconoGS' icon='my-icons:description'></iron-icon>S</p>
            <p><iron-icon class='iconoFS' icon='my-icons:dns'></iron-icon>F</p>
          </div>
          <template is="dom-repeat" items="{{listaNoSincronizados}}">
            <div class='datosNoSincroTitulo listaRespuesta' 
              style=' display: grid; grid-template-columns: 1fr 1fr 1fr; color: black; text-align: center; 
              margin: 5px; align-items: center; justify-items: center;'>
              <input type='button' value='[[item.placa]]' on-click='_datosPlaca' class='botonPlaca'></input>
              <p>[[item.GS]]</p>
              <p>[[item.FS]]</p>
            </div>
          </template>
        </template>
      </div>

      <!-- Eliminar viaje -->
      <paper-dialog id="eliminarViaje"> 
        <p>Hola viaje</p>
      </paper-dialog>
    `
  }

  static get properties () {
    return {
      baseDatos: Object,
      comprobacionGS: Boolean,
      botonComprobar: {
        type: Boolean,
        value: true
      },
      textoComprobar: {
        type: String,
        value: 'Comprobar'
      },
      datosNoSincronizados: {
        type: Boolean,
        value: false
      },
      botonInfo: {
        type: Boolean,
        value: true
      },
      infoTextoProceso: {
        type: String,
        value: 'Obtener'
      }
    }
  } // *** INFORMACION ***

  _infoPlaca () {
    this.infoTextoProceso = 'Procesando'
    this.updateStyles({
      '--icono-informacion-color': 'blue'
    })

    this._pedirInfo()
  } // *** COMPROBACION ***

  _reiniciarComprobacion () {
    this.datosSincronizados = false
    this.reportarRevision = false
    this.botonComprobar = true
    this.textoComprobar = 'Comprobar'
    this.updateStyles({
      '--icono-comprobar-color': 'rgba(97,97,97)'
    })
    this.datosNoSincronizados = false
  }

  _comprobacion () {
    // senal de la accion
    this.textoComprobar = 'Verificando'
    this.updateStyles({
      '--icono-comprobar-color': 'blue'
    }) // conexion con spreadsheet

    this._conexionGS()
  }

  _conexionGS () {
    const CLIENT_ID = '182689894492-b2jsok6plterqsk9gukq4c2n86isdfic.apps.googleusercontent.com'
    const API_KEY = 'AIzaSyAYNrBMc_v6fDk6qRAJPsavZMwPWZi10NI'
    const DISCOVERY_DOCS = ['https://sheets.googleapis.com/$discovery/rest?version=v4']
    const SCOPES = 'https://www.googleapis.com/auth/spreadsheets'
    gapi.load('client:auth2', this._peticionGS(CLIENT_ID, API_KEY, DISCOVERY_DOCS, SCOPES))
  }

  async _peticionGS (CLIENT_ID, API_KEY, DISCOVERY_DOCS, SCOPES) {
    // Configuracion del cliente
    await gapi.client.init({
      apiKey: API_KEY,
      clientId: CLIENT_ID,
      discoveryDocs: DISCOVERY_DOCS,
      scope: SCOPES
    })

    // ** Buscar Rango de analisis segun dia

    const dias = await gapi.client.sheets.spreadsheets.values.get({
      spreadsheetId: '1_WdtyUW4bQ2og-AJ29Kd4AT0BqhgszVxAzWQ8xYMwtQ',
      range: 'Viajes!A1:ZZZ1'
    })
    // convertir yy/mm/dd a dd/mm/yy

    const diaYy = this.diaDefecto.split(/\D/g)
    const diaDd = [diaYy[2], diaYy[1], diaYy[0]].join('/') // buscar posicion en GS

    const posicion = dias.result.values[0].indexOf(diaDd) // ** Peticion de informacion
    // info de GS, filtrado

    const datosGS = await this._comprobacionDatosGS(posicion) // info de FS, filtrado

    const datosFS = await this._comprobacionDatosFS(diaYy) // ** Comparacion

    this._comparacionGSFS(datosGS, datosFS)
  } // Datos desde GS

  async _comprobacionDatosGS (posicion) {
    // obtener
    const objeto = await gapi.client.sheets.spreadsheets.values.get({
      spreadsheetId: '1_WdtyUW4bQ2og-AJ29Kd4AT0BqhgszVxAzWQ8xYMwtQ',
      range: `Viajes!A3:ZZZ`,
      majorDimension: 'COLUMNS'
    })
    // Organizar
    const datosCompletos = objeto.result.values
    const datosOrganizados = Array.from({
      length: datosCompletos[1].length
    }, (x, i) => {
      return {
        placa: datosCompletos[1][i],
        viajes: datosCompletos[posicion][i]
      }
    })
    // filtrar y retornar
    return datosOrganizados.filter(x => x.placa !== '' && x.viajes !== '' && x.viajes !== '0' && x.viajes !== undefined)
  } // Datos desde FS

  async _comprobacionDatosFS (diaYy) {
    // valores de fecha
    const inicio = new Date(diaYy.join())
    const fin = new Date(inicio.getTime() + 24 * 60 * 60 * 1000)
    // datos FS
    const datosFS = await this.baseDatos.collection('datos').where('tiempo', '>', inicio).where('tiempo', '<', fin).get()
    const datosCompletos = datosFS.docs.map(x => x.data())
    // filtrado
    const filtroEliminados = datosCompletos.filter(x => x.eliminado === '')
    // Sumatoria de viajes por volqueta
    const listaXPlaca = filtroEliminados.map(x => x.placa)
    const sumaViajes = {}
    listaXPlaca.forEach(x => {
      sumaViajes[x] = (sumaViajes[x] || 0) + 1
    })
    // retorno de placas y viajes
    return Object.keys(sumaViajes).map((x, i) => {
      return {
        placa: x,
        viajes: sumaViajes[x].toString()
      }
    })
  }
  // Comparacion

  _comparacionGSFS (datosGS, datosFS) {
    // ** Lista de volquetas
    const listaGS = datosGS.map(x => x.placa)
    const listaFS = datosFS.map(x => x.placa)
    // ** Lista de viajes diferentes
    // buscar diferencias
    const datosIguales = listaGS.filter(x => listaFS.includes(x))
    const datosDiferentes = datosIguales.filter(x => datosGS[listaGS.indexOf(x)].viajes !== datosFS[listaFS.indexOf(x)].viajes)
    // ** Mostrar mensaje de sincronizado
    if (listaGS.length === listaFS.length && !datosDiferentes.length) {
      // No hay errores
      this.datosSincronizados = true
      this.botonComprobar = false
      return
    }
    // ** Mostrar info con errores
    const datoViaje = (lista, placas, x) => {
      let respuesta = ''
      try {
        respuesta = lista[placas.indexOf(x)].viajes
      } catch (e) {
        respuesta = 0
      }

      return respuesta
    }
    // Organizar info
    const datosUnicos = listaGS.filter(x => !listaFS.includes(x)).concat(listaFS.filter(x => !listaGS.includes(x)))
    const placas = [...new Set([...datosDiferentes, ...datosUnicos])]
    this.listaNoSincronizados = Array.from(placas, x => {
      return {
        placa: x,
        GS: datoViaje(datosGS, listaGS, x),
        FS: datoViaje(datosFS, listaFS, x)
      }
    })
    // Cambiar icono
    this.reportarRevision = true
    this.botonComprobar = false
    this.datosNoSincronizados = true
  }
  // Mostrar datos de la placa

  _datosPlaca () {
    alert(1)
  } // *** Inicio de la aplicacion ***

  async ready () {
    super.ready() // dia por defecto

    const opciones = {
      year: 'numeric',
      month: '2-digit',
      day: '2-digit'
    }
    this.diaDefecto = new Date().toLocaleDateString('ja-JP', opciones).replace(/\//g, '-')
  }
}

window.customElements.define('opc-comprobar', OpcComprobar)
