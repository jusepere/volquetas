import { LitElement, html, css } from 'lit-element'

class ConLugares extends LitElement {
  static get styles () {
    return css`
      .h {
        margin: 1px
      }

      .listaLugaresContenedor{
        display: grid;
        place-content: center
      }
    `
  }

  render () {
    return html`
      <div class='listaLugaresContenedor'>
        <input value='Canal Figueroa'></input>
      </div>
    `
  }
}

// eslint-disable-next-line no-undef
customElements.define('con-lugares', ConLugares)
