import express from 'express'
import * as path from 'path'
import bodyParser from 'body-parser'
import low from 'lowdb'
import FileAsync from 'lowdb/adapters/FileAsync'

window.navigator.onLine ? console.log('firebase') : console.log('no hay')
// *** SERVIDOR ***
const app = express()
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(express.static(path.join('C:/Users/Geo/Desktop/Proyectos/volquetas', '/build/defecto')))
const server = app.listen(3000, '127.0.0.1', () => {
  let infoServe = server.address()
  console.log(`Server: http://${infoServe.address}:${infoServe.port}`)
})

// *** BASE DE DATOS ***
const baseDatos = async () => {
  // ** Inicio **
  const adapter = new FileAsync('baseDatos/volquetas.json')
  const db = await low(adapter)
  db.defaults({ datos: [], favoritos: [], registro: [] }).write()

  // ** Peticiones **
  // Listado
  app.post('/listado', (req, res) => {
    // se asigna informacion a columna creada
    res.send(db.get('volqueta').value())
  })
}
baseDatos()
