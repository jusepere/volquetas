import { PolymerElement, html } from '@polymer/polymer/polymer-element.js'
import '@polymer/paper-input/paper-input.js'
import './shared-styles.js'

class ConIngreso extends PolymerElement {
  static get template () {
    return html`
      <style include='shared-styles'>
        /* ---  ---*/
        :host {
          padding: 5px;
        }

        .botones {
          display:flex;
          justify-content: space-between;
          align-items: center;
          margin-top:10px
        }

        .card {
          width:210px
        }

        .inputMenor {
          width: 125px
        }

        h4 {
          color: black
        }

      </style>

      <!-- Ingreso de placa -->
        <div class='card'>
          <div class='botones'>
            <iron-icon icon='my-icons:close' style='color:#7f0000' ></iron-icon>
            <h4>Ingreso de volqueta</h4>
            <iron-icon icon='my-icons:save' style='color:#3F51B5' ></iron-icon>
          </div>
          <paper-input id='placa' class='inputMenor' value='{{placa}}' label='Placa' maxlength='6'></paper-input>
          <paper-input id='m³' class='inputMenor' value='{{m}}' label='m³'></paper-input>
          <paper-input id='empresa' class='busqueda' value='{{empresa}}' label='Empresa'></paper-input>
        </div>

        <div class='card'>
          <div class='botones'>
          <iron-icon icon='my-icons:close' style='color:#7f0000' ></iron-icon>
          <h4>Ingreso de botadero</h4>
          <iron-icon icon='my-icons:save' style='color:#3F51B5' ></iron-icon>
        </div>
        <paper-input id='botadero' class='busqueda' value='{{botadero}}' label='Nombre'></paper-input>
    `
  }

  static get properties () {
    return {
    }
  }
}

window.customElements.define('con-ingreso', ConIngreso)
