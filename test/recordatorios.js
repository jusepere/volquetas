// *** Abrir una venta o un paper dialog de varias opciones

<div class='card menu'>
  <iron-icon id='iccomparacionGS' class='iconomenu' icon='my-icons:compare-arrows' on-click='_ventanaOpciones'></iron-icon>
  <iron-icon id='icinfoCodigo' class='iconomenu' icon='my-icons:zoom-in' on-click='_ventanaOpciones'></iron-icon>
  <iron-icon id='icpruebaImpresora' class='iconomenu' icon='my-icons:print' on-click='_ventanaOpciones'></iron-icon>
</div>

_ventanaOpciones (e) {
    const tipo = e.target.id.slice(2)
    this.shadowRoot.querySelector(`#${tipo}`).open()
}


// Ingresar una propiedad a cada codigo en FIREBASE - OJO MAXIMO 500 DATOS - 
async _ingresarCodigo () {
  this.updateStyles({
    '--icono-comprobar-color': 'blue'
  })
  const datosCompletos = await this.baseDatos.collection('datos').get()
  const ids0 = datosCompletos.docs.map(x => x.id)
  console.log(ids0.length)
  const ids = ids0.slice(5008, 5447)
  const batch = this.baseDatos.batch()
  ids.map((x) => {
    batch.update(this.baseDatos.collection('datos').doc(x), {
      // dia: diaPrueba
      codigo: x
    })
  })
  batch.commit()
  console.log('finalizo')
}