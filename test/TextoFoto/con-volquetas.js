import { PolymerElement, html } from "../node_modules/@polymer/polymer/polymer-element.js";
import "../node_modules/@polymer/paper-input/paper-input.js";
import "../node_modules/@polymer/paper-dialog/paper-dialog.js";
import "../node_modules/@polymer/paper-radio-button/paper-radio-button.js";
import "../node_modules/@polymer/paper-radio-group/paper-radio-group.js";
import firebase from "../node_modules/firebase/app/dist/index.esm.js";
import "../node_modules/firebase/firestore/dist/index.esm.js";
import "../node_modules/@polymer/iron-ajax/iron-ajax.js";
import './shared-styles.js';

class ConVolquetas extends PolymerElement {
  static get template() {
    return html`
      <style include='shared-styles'>
        /* ---  ---*/
        :host {
          display: block;
          padding: 10px;
        }

        /* --- Ingreso ---*/
        .iniIngreso {
          display: flex;
          justify-content: center;
          margin-top:-10px;
        }

        .busqueda {
          max-width: 145px;
          margin-top:-35px;
          --paper-input-container: {
            font-size: 18px;
            margin-left: 10px;
          }
        }

        .nuevaPlacaContenedor{
          display: none;
          margin-top:10px;
          align-items: center;
          justify-content: space-around;
          flex-wrap: wrap

        }

        .nuevaPlaca {
          width: 70px;
          --paper-input-container: {
            font-size: 18px;
          }
        }

        .nuevoM3 {
          max-width: 60px;
        }

        .nuevaPlacaCabezera {
          display:flex;
          align-itmes:center;
          width: 100%;
          color:black;          
          margin-bottom:5px;
          justify-content: space-between
        }

        .nuevaPlacaTitulo {
          margin:0px;
        }
        
        .placaBoton {
          background-color: white;
          font-size: 16px;
          border: 1px solid black;
          border-radius: 8%;
          margin:5px;
        }

        .contenedor {
          display: flex;
          flex-wrap: wrap;
          justify-content: space-around
        }

        .iniDatos {
          display: flex;
          flex-wrap: wrap;
          justify-content: space-between
        }

        .varDatos {
          display: flex;
          justify-content: space-between;
          flex-wrap:wrap;
          margin-top:5px;
          margin-bottom:20px
        }

        .datos {
          display: none
        }

        .m³ {
          max-width: 55px;
          color: black;
          --paper-input-container-input: {
            font-size: 18px;
          };
          --paper-input-container-label: {
            font-size: 18px;
          };
        }

        .botadero {
          --paper-input-container-label: {
            font-size: 18px;
          };
        }

        .observaciones {
          color: black;
          width: 70%;
          --paper-input-container-label: {
            font-size: 18px;
          };
          
        }

        .fotoContenedor {
          width: 260px; 
          height: 260px; 
          background: grey; 
          margin: 0 auto
        }

        .fotoInicio {
          display: flex; 
          height: 100%; 
          justify-content: center; 
          align-items: center
        }
               
        .texto {
          margin-bottom: 45px;
          margin-top: -50px;
          width: 100%;
          color: yellow;
        }

        paper-dialog {
          min-width: 321px;
          max-width: 321px
        }

        .dispoControl {
          display: flex
        }

        .dispoEliminar {
          max-width: 80px;
          margin-left: 20px
        }

        .dispoContenedor {
          overflow: auto;
          height: 200px
        }
      </style>

      <!-- Ingreso de placa -->
      <div class='card'>
        <div class='iniIngreso'>
          <paper-input 
            id='busqueda' 
            class='busqueda' 
            value='{{busqueda}}' 
            placeholder='Placa' 
            on-click='_verBusqueda' 
            maxlength='6'>
            <iron-icon icon='my-icons:search' slot='prefix'></iron-icon>
            <paper-icon-button slot='suffix' on-click='_limpiar' icon='my-icons:close' style='color:#7f0000'>
            </paper-icon-button>
          </paper-input>
        </div>
        <div id='contenedor' class='contenedor'>
          <template is='dom-repeat' items={{placas}}>
            <input id='[[item]]' class='placaBoton' type='button' value='[[item]]' on-click='_ingreso'></input>
          </template>
          <array-selector id="selector" items="{{placas}}" selected="{{placaSeleccion}}"></array-selector>
        </div>
        <div id='nuevaPlacaContenedor' class='nuevaPlacaContenedor'>
          <div class='nuevaPlacaCabezera'>
            <p class='nuevaPlacaTitulo'>Registrar:</p>
            <select id='selecEmpresa'>
              <option value="Contratista">Contratista</option>
              <option value="Coningenieria">Coningenieria</option>
            </select>
            <iron-icon icon='my-icons:save' style='color:#004D40' on-click='_nuevaVolqueta'></iron-icon>
          </div>  
          <paper-input 
            id='nuevaPlaca' 
            class='nuevaPlaca' 
            value='{{nuevaPlaca}}' 
            always-float-label label='Nueva placa' 
            auto-validate
            maxlength='6'
            pattern='[a-zA-Z]{3}[0-9]{3}'
            error-message='ABC123'>
          </paper-input>
          <paper-input 
            id='nuevoM3' 
            class='nuevoM3'
            value='{{nuevoM3}}'
            type='number'
            always-float-label label='Cubicaje'
            auto-validate
            pattern='^[1-9][0-9]*$'
            error-message='no válido'>
            <div slot='suffix' style='color:black'>m³</div>
          </paper-input>
        </div>
      </div>

      <!-- Ingreso de datos y foto -->
      <div id='datos' class='card datos'>
        <div class='iniDatos'>
          <iron-icon 
            id='favoritoIcono' 
            icon='my-icons:star-border' 
            style='color:#004D40' 
            slot='prefix'
            on-click='_favorito'></iron-icon>
          <h3 style='color:black; margin: 0'> {{placa}} | d: {{dia}} - c: {{corte}} </h3>
          <iron-icon icon='my-icons:save' on-click='_guardar' style='color:#3F51B5' slot='prefix'></iron-icon>
        </div>
        <div class='varDatos'>
          <paper-input id='datosM'  class='m³' always-float-label label='Cantidad' value='{{m3}}'>
            <div slot='suffix'>m³</div>
          </paper-input>
          </paper-dropdown-menu>
          <paper-input 
            id='datosObser' 
            class='observaciones' 
            always-float-label label='Observaciones' 
            value={{observaciones}}>
          </paper-input>
          <paper-input id='datosDispo' class='observaciones' always-float-label label='Disposición' value='{{dispoElegido}}' readonly="readonly">
            <paper-icon-button slot='suffix' on-click='_dispoBuscar' icon='my-icons:search'>
            </paper-icon-button>
          </paper-input>
        </div>
        <div id='fotoContenedor' class='fotoContenedor'>
          <template is="dom-if" if="{{fotoVista}}">
            <div id='fotoInicio' class='fotoInicio' on-click='_fotoVista'>
                <iron-icon icon='my-icons:camera-enhance' style='color:white'></iron-icon>
            </div> 
          </template>
          <template is="dom-if" if="{{!fotoVista}}">
            <img src="data:image/png;base64,{{foto}}" on-click='_foto'>
            <p class='texto'>{{datos1}}</p>
            <p class='texto'>{{datos2}}</p>
          </template>          
        </div>
      </div>

      <paper-dialog id="dispoBuscar">
        <div class='dispoContenedor'>
          <paper-radio-group selected="small">
            <template is='dom-repeat' items={{disposicion}} mutable-data>
              <paper-radio-button name='[[item]]' on-click='_dispoEleccion'>[[index]]. [[item]]</paper-radio-button>
            </template>
          </paper-radio-group>
        </div>
        <div class='dispoControl'>
          <paper-input 
            id='datosDispo' 
            class='observaciones' 
            always-float-label label='Adicionar' 
            value='{{dispoAdicionar}}'>
            <paper-icon-button 
              slot='suffix' 
              on-click='_dispoAdicionar' 
              icon='my-icons:add-circle' 
              style='color:#3F51B5'>
            </paper-icon-button>
          </paper-input>
          <paper-input class='dispoEliminar' type='number' always-float-label label='Eliminar #' value='{{dispoEliminar}}' >
            <paper-icon-button 
              slot='suffix' 
              on-click='_dispoEliminar' 
              icon='my-icons:close' 
              style='color:#7f0000'>
            </paper-icon-button>
          </paper-input>
        </div>
      </paper-dialog>

      <!-- PETICIONES -->
      
      <!-- foto -->
      <iron-ajax 
        id='camaraBd' 
        url='http://192.168.1.104:3000/camara' 
        method='post' 
        body={{camaraDatos}}
        handle-as="text"
        on-response="_camaraRes">
      </iron-ajax>

      <!-- guardar -->
      <iron-ajax 
        id='guardarFoto' 
        url='http://192.168.1.104:3000/guardar' 
        method='post'
        content-type="application/json"
        body={{fotoBd}}>
      </iron-ajax>
      
      <!-- imprimir volqueta -->
      <iron-ajax 
        id='imprimir' 
        url='http://192.168.1.104:3000/imprimir' 
        method='post'
        content-type="application/json"
        body={{infoRecibo}}
        on-response='_imprimirDispo'>
      </iron-ajax> 
      
      <!-- imprimir disposicion -->
      <iron-ajax 
        id='imprimirDispo' 
        url='http://192.168.1.104:3000/imprimirDispo' 
        method='post'
        content-type="application/json"
        body={{infoDispo}}>
      </iron-ajax> 
    `;
  }

  static get properties() {
    return {
      busqueda: {
        type: String,
        observer: '_buscar'
      },
      fotoVista: {
        type: Boolean,
        value: true
      },
      foto: {
        type: Boolean,
        value: false
      },
      observaciones: {
        type: String,
        value: ''
      },
      tituloDia: {
        type: Number,
        notify: true
      },
      tituloM3: {
        type: Number,
        notify: true
      },
      vectorReg: {
        type: Array,
        value: ['a', 'Y', '%', 'o', 'E', '5', 'w', 't', '_', 'U', '~', 'S', 'H', '{', 'r', 'x', 'g', 'l', 'n', 'A', 'L', 'b', '+', 'C', '@', '3', '1', '4', '^', '9', 'd', 'q', 'Q', 'G', '¿', 'K', 'u', 'T', 'v', '8', '"', 'B', 's', 'k', '&', 'R', ';', 'W', '$', 'P', '}', 'J', '*', ',', 'z', 'h', 'X', '<', 'i', 'y', 'M', '[', '>', '?', '=', 'I', ')', '¡', 'Z', 'm', ']', 'F', '|', '0', 'e', '!', '6', 'D', 'f', '-', '(', '2', 'N', 'p', 'V', 'c', '.', 'O', ':', '7']
      }
    };
  } // *** Inicio de la aplicacion ***


  constructor() {
    super();

    this._firebase(); // window.navigator.onLine ? console.log('firebase') : console.log('no hay')

  }

  async _firebase() {
    firebase.initializeApp({
      apiKey: 'AIzaSyAYNrBMc_v6fDk6qRAJPsavZMwPWZi10NI',
      authDomain: 'volquetas-1.firebaseapp.com',
      databaseURL: 'https://volquetas-1.firebaseio.com',
      projectId: 'volquetas-1',
      storageBucket: 'volquetas-1.appspot.com',
      messagingSenderId: '182689894492'
    });
    this.db = firebase.firestore();
    this.db.settings({
      timestampsInSnapshots: true
    });
    this.db.enablePersistence(); // peticiones al firebase

    const [fav, doc, m3Temp, reg, dispo] = await Promise.all([this.db.collection('listas').doc('favoritos').get(), this.db.collection('volquetas').get(), this.db.collection('listas').doc('m3').get(), this.db.collection('listas').doc('registro').get(), this.db.collection('disposicion').get()]); // Inicio de datos
    // control de viajes

    await this._cortes(doc, dispo); // favoritos

    this.placas = this.favoritos = fav.data().lista; // datos

    this.placasBd = doc.docs.map(x => {
      return {
        'placa': x.id,
        'm3': x.data().m3,
        'empresa': x.data().empresa
      };
    }); // valores diarios

    this.tituloDia = doc.docs.map(x => x.data().dia).reduce((x, y) => x + y);
    this.tituloM3 = m3Temp.data().m3; // registro

    this.registro = reg.data().lista; // disposicion

    this.disposicion = dispo.docs.map(x => x.id);
  }

  async _cortes(doc, dispo) {
    // Comprobación día
    const diaLocal = new Date(new Date().setHours(0, 0, 0, 0)).getTime();
    const datos = await this.db.collection('listas').doc('cortes').get();
    const diaBd = datos.data().dia.toDate().setHours(0, 0, 0, 0);
    const batch = this.db.batch(); // reinicio dia y viajes diarios en Bd

    if (diaLocal > diaBd) {
      this.db.collection('listas').doc('cortes').update({
        dia: new Date(diaLocal)
      });
      doc.docs.map(x => batch.update(x.ref, {
        dia: 0
      }));
      dispo.docs.map(x => batch.update(x.ref, {
        dia: 0
      }));
      this.db.collection('listas').doc('m3').set({
        m3: 0
      });
    } // ** Comprobacion de cortes


    const dia = new Date().getDate(); // Corte volquetas

    const corteVolq = datos.data().corteVolqueta;

    if (dia === corteVolq[0] + 1 || dia === corteVolq[1] + 1) {
      // reinicio viajes corte en Bd
      doc.docs.map(x => batch.update(x.ref, {
        corte: 0
      }));
    } // Corte dispo


    const corteDispo = datos.data().corteDispo;

    if (dia === corteDispo) {
      dispo.docs.map(x => batch.update(x.ref, {
        corte: 0
      }));
    }

    batch.commit();
  } // *** Buscador de placas ***


  _verBusqueda() {
    this.$.contenedor.style.display = 'flex';
    this.$.datos.style.display = 'none';
    this.dia = '';
    this.corte = '';
  }

  async _ingreso(e) {
    // Vista
    this.$.contenedor.style.display = 'none';
    this.$.nuevaPlacaContenedor.style.display = 'none';
    this.$.datos.style.display = 'block';
    this.favoritos.includes(e.currentTarget.value) ? this.$.favoritoIcono.icon = 'my-icons:star' : this.$.favoritoIcono.icon = 'my-icons:star-border'; // titulo

    this.placa = e.currentTarget.value; // cubicaje por defecto

    const datos = this.placasBd.find(x => x.placa === this.placa);
    this.m3 = datos.m3;
    this.empresa = datos.empresa; // # salidas volquetas

    const viajes = await this.db.collection('volquetas').doc(this.placa).get();
    this.dia = viajes.data().dia;
    this.corte = viajes.data().corte;
    this.total = viajes.data().total;
  }

  _buscar() {
    // se muestra favoritos
    const textoPlaca = this.busqueda.toUpperCase();
    this.nuevaPlaca = textoPlaca;

    if (this.busqueda === '') {
      this.placas = this.favoritos;
      this.$.nuevaPlacaContenedor.style.display = 'none';
    } else {
      // se muestra busqueda
      const placasFiltradas = this.placasBd.filter(x => x.placa.includes(textoPlaca));

      if (placasFiltradas.length > 0) {
        this.placas = placasFiltradas.map(x => x.placa);
        this.$.nuevaPlacaContenedor.style.display = 'none';
      } else {
        // se muestra ingreso de nueva placa
        this.placas = '';
        this.$.nuevaPlacaContenedor.style.display = 'flex';
      }
    }
  }

  _nuevaVolqueta() {
    this.nuevaPlaca = this.nuevaPlaca.toUpperCase();
    const patronPlaca = new RegExp('[a-zA-Z]{3}[0-9]{3}');
    const patronM3 = new RegExp('^[1-9][0-9]*$');

    if (patronPlaca.test(this.nuevaPlaca) && patronM3.test(this.nuevoM3)) {
      const empresa = this.$.selecEmpresa;
      this.db.collection('volquetas').doc(this.nuevaPlaca).set({
        corte: 0,
        dia: 0,
        empresa: empresa.options[empresa.selectedIndex].text,
        m3: Number(this.nuevoM3),
        total: 0
      }); // termino proceso

      this.placasBd.push({
        placa: this.nuevaPlaca,
        m3: Number(this.nuevoM3),
        empresa: empresa.options[empresa.selectedIndex].text
      });
      this.nuevaPlaca = this.nuevoM3 = this.busqueda = '';
      this.$.nuevaPlacaContenedor.style.display = 'none';
    } else {
      // eslint-disable-next-line no-undef
      alert('Revisar información de ingreso');
    }
  }

  _favorito() {
    // Se quita de favoritos
    if (this.favoritos.includes(this.placa)) {
      this.favoritos = this.favoritos.filter(x => x !== this.placa);
      this.db.collection('listas').doc('favoritos').set({
        lista: this.favoritos
      });
      this.$.favoritoIcono.icon = 'my-icons:star-border';
    } else {
      // se agrega a favoritos
      this.favoritos.push(this.placa);
      this.db.collection('listas').doc('favoritos').set({
        lista: this.favoritos
      });
      this.$.favoritoIcono.icon = 'my-icons:star';
    } // actualizo vista


    this.placas = this.favoritos;
  } // *** Disposicion ***


  _dispoBuscar() {
    this.$.dispoBuscar.open();
  }

  _dispoEleccion(e) {
    this.dispoElegido = e.target.name;
    this.$.dispoBuscar.close();
  }

  _dispoAdicionar() {
    if (this.dispoAdicionar !== '') {
      this.push('disposicion', this.dispoAdicionar);
      this.db.collection('disposicion').doc(this.dispoAdicionar).set({
        corte: 0,
        dia: 0,
        total: 0
      });
    }

    this.dispoAdicionar = '';
  }

  _dispoEliminar() {
    if (this.dispoEliminar !== '' && this.dispoEliminar !== undefined && this.dispoEliminar <= this.disposicion.length && Number(this.dispoEliminar) >= 0) {
      this.db.collection('disposicion').doc(this.disposicion[this.dispoEliminar]).delete();
      this.splice('disposicion', this.dispoEliminar, 1);
    }
  } // *** Foto ***


  _fotoVista() {
    // Control de vista
    this.fotoVista = !this.fotoVista;
    this.foto = !this.foto;

    this._foto();
  }

  _foto() {
    if (this.dispoElegido === '') {
      // eslint-disable-next-line
      alert('ingresar lugar de disposicón'); // Control de vista

      this.fotoVista = !this.fotoVista;
      this.foto = !this.foto;
      return;
    }

    this.$.camaraBd.generateRequest(); // Texto en foto

    this.tiempo = new Date();
    this.fecha = ` ${this.tiempo.getDate()}/${this.tiempo.getMonth() + 1}/${this.tiempo.getFullYear()} `;
    this.hora = ` ${this.tiempo.getHours()}:${this.tiempo.getMinutes()}:${this.tiempo.getSeconds()} `;
    this.datos1 = ` ${this.placa} -  ${this.fecha} - ${this.hora} `;
    this.datos2 = `${this.m3} m³ - ${this.dia + 1} día - ${this.corte + 1} corte - ${this.dispoElegido}`;
  }

  async _camaraRes(e) {
    this.foto = e.detail.response;
    const node = this.$.fotoContenedor
    const foto = await domtoimage.toPng(node)
    this.fotoBd = {
      foto: foto.substring(22)
    };
  } // *** Impresora ***


  _imprimirDispo() {
    this.infoDispo = {
      dispo: this.dispoElegido,
      placa: this.placa,
      fecha: this.fecha,
      dia: this.diaDispo,
      corte: this.corteDispo,
      codigo: this.unico
    };
    this.$.imprimirDispo.generateRequest();
  }

  _imprimir() {
    // informacion
    this.infoRecibo = {
      codigo: this.unico,
      placa: this.placa,
      empresa: this.empresa,
      fecha: this.fecha,
      hora: this.hora,
      dia: this.dia,
      corte: this.corte,
      dispo: this.dispoElegido,
      m3: this.m3
    };
    this.$.imprimir.generateRequest();
  } // *** Funcionalidades ***


  async _guardar() {
    if (!this.foto || this.dispoElegido === '') {
      // eslint-disable-next-line no-undef
      alert('Completar información');
      return;
    } // Se genera registro unico
    // eslint-disable-next-line


    const f = x => i => x[i] + 1 <= this.vectorReg.length ? (x[i] = x[i] + 1, x) : (x[i] = 0, f(x)(i - 1));

    this.registro = f(this.registro)(2); // se registra en Firestore

    this.db.collection('listas').doc('registro').set({
      lista: this.registro
    });
    this.unico = this.registro.map(x => this.vectorReg[x]).join(''); // se guarda la info

    this.db.collection('datos').doc(this.unico).set({
      placa: this.placa,
      tiempo: this.tiempo,
      dia: Number(this.dia) + 1,
      corte: Number(this.corte) + 1,
      disposicion: this.dispoElegido,
      observaciones: this.observaciones,
      m3: this.m3,
      empresa: this.empresa
    }); // guarda foto localmente

    this.fotoBd['codigo'] = this.unico;
    this.$.guardarFoto.generateRequest(); // actualiza volqueta

    this.db.collection('volquetas').doc(this.placa).update({
      dia: Number(this.dia) + 1,
      corte: Number(this.corte) + 1,
      total: Number(this.total) + 1
    }); // actualiza disposicion

    const tempDispo = await this.db.collection('disposicion').doc(this.dispoElegido).get();
    const {
      corte,
      dia,
      total
    } = tempDispo.data();
    this.db.collection('disposicion').doc(this.dispoElegido).update({
      dia: Number(dia) + 1,
      corte: Number(corte) + 1,
      total: Number(total) + 1
    });
    this.diaDispo = Number(dia) + 1;
    this.corteDispo = Number(corte) + 1; // imprime recibo

    this._imprimir(); // actualiza contador local


    this.tituloDia++;
    this.tituloM3 = Number(this.tituloM3) + Number(this.m3);
    this.db.collection('listas').doc('m3').set({
      m3: this.tituloM3
    }); // Vista

    this._verBusqueda();

    this._fotoVista();
  }

  _limpiar() {
    this.busqueda = '';
  }

}

window.customElements.define('con-volquetas', ConVolquetas);
