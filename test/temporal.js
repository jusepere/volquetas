const datosGS = [{ placa: 'WCS603', viajes: '3' }, { placa: 'WCS605', viajes: '3' }, { placa: 'WCS606', viajes: '2' },
  { placa: 'WNM335', viajes: '3' }, { placa: 'WNM334', viajes: '7' }, { placa: 'THY219', viajes: '5' }]
const datosFS = [{ placa: 'AAA111', viajes: '5' }, { placa: 'WNM335', viajes: '3' }, { placa: 'WCS603', viajes: '4' },
  { placa: 'WNM334', viajes: '3' }, { placa: 'WCS606', viajes: '2' }, { placa: 'THY219', viajes: '3' }, { placa: 'WCS605', viajes: '3' }]
// Obtener todas las placas de analisis
const listaGS = datosGS.map(x => x.placa)
const listaFS = datosFS.map(x => x.placa)
const datos = [...new Set([...listaGS , ...listaFS])]

const datoViaje = (lista, placas , x) => {
  let respuesta = ''
  try {
    respuesta = datosGS[placas.indexOf(x)].viajes
  } catch (e) {
    respuesta = '-'
  }
  return respuesta
}
const tt = datoViaje(datosGS, listaGS, 'WNM334')

const t = Array.from(datos, x => {
  return { placa: x, GS: datoViaje(datosGS, listaGS, x), FS: datoViaje(datosFS, listaFS, x) }
})
t
const g = [1, 2, 3]
const gg = [1, 3, 6]
const ggg = g.filter(x => !gg.includes(x)).concat(gg.filter(x => !g.includes(x)))
ggg
