var p = new Promise((resolve, reject) => resolve(5))
async function prueba () {
  const t = await p
  console.log(t)
}

// const internetAvailable = require('internet-available')

// async function prueba () {
//   try {
//     await internetAvailable({ timeout: 1000, retries: 2 })
//     console.log('res')
//   } catch (error) {
//     console.log('error')
//   }
// }

prueba()

// .then(() => { console.log(1) })
// .catch(() => { console.log(0) })
